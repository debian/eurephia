/* lastlog.c   --  eurephiadm lastlog command:
 *                 Queries the lastlog table
 *
 *  GPLv2 only - Copyright (C) 2009 - 2012
 *               David Sommerseth <dazo@users.sourceforge.net>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; version 2
 *  of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

/**
 * @file   eurephiadm/commands/lastlog.c
 * @author David Sommerseth <dazo@users.sourceforge.net>
 * @date   2009-03-24
 *
 * @brief  eurephiadm lastlog command.  Queries the lastlog database.
 *
 */

#include <stdio.h>
#include <string.h>
#include <assert.h>

#ifdef HAVE_LIBXML2
#include <libxml/tree.h>
#include <libxml/xpath.h>
#endif

#define MODULE "eurephia::LastLog" /**< Need to define the active module before including argparser.h */
#include <eurephia_nullsafe.h>
#include <eurephia_context.h>
#include <eurephia_log.h>
#include <eurephia_xml.h>
#include <eurephia_values_struct.h>
#include <eurephiadb_session_struct.h>
#include <eurephiadb_mapping.h>
#include <eurephiadb_driver.h>
#include <certinfo.h>

#include "../argparser.h"
#include "../xsltparser.h"


/**
 *  Help screen for the lastlog command
 */
void help_Lastlog()
{
        printf("eurephiadm::Lastlog\n\n"
               "This command will query the lastlog entries, which contains information\n"
               "about all logins done with the eurephia-auth plug-in.\n\n"
               "Valid arguments:\n"
               "  * Filters:\n"
               "     -c | --certid      Certificate ID\n"
               "     -i | --uid         User ID\n"
               "     -u | --username    User name\n"
               "     -I | --ip-addr     IP address of remote host\n"
               "     -s | --login       Login time\n"
               "     -e | --logout      Logout time\n"
               "     -m | --mac-addr    MAC address of remote VPN interface\n"
               "     -a | --uicid       Access profile ID\n"
               "\n"
               "  * Other arguments:\n"
               "     -S | --sortkeys    List sorting fields\n"
               "     -v | --verbose     View detailed lastlog\n"
               "\n"
               "    Valid sort keys are: uid, certid, ip, vpnip, status, login, logout,\n"
               "                         username, macaddr, uicid\n"
               "\n"
               );
}


/**
 *  Main function for the lastlog command.  Does the querying of the database and show the result
 *
 * @param ctx   eurephiaCTX
 * @param sess  eurephiaSESSION of the current logged in user
 * @param cfg   eurephiaVALUES struct of the current configuration
 * @param argc  argument count for the eurephiadm command
 * @param argv  argument table for the eurephiadm command
 *
 * @return returns 0 on success, otherwise 1.
 */
int cmd_Lastlog(eurephiaCTX *ctx, eurephiaSESSION *sess, eurephiaVALUES *cfg, int argc, char **argv)
{
        xmlDoc *lastlog_xml = NULL, *srch_xml = NULL;
        xmlNode *fmap_n = NULL, *srch_n = NULL, *flt_n = NULL;
        int i = 0;
        char *sortkeys = NULL;
#ifdef FIREWALL
        char *xsltparams[] = {"view", "'list'", "firewall", "'1'", NULL};
#else
        char *xsltparams[] = {"view", "'list'", "firewall", "'0'", NULL};
#endif

        e_options listargs[] = {
                {"--certid",     "-c", 1},
                {"--uid",        "-i", 1},
                {"--username",   "-u", 1},
                {"--ip-addr",    "-I", 1},
                {"--vpn-ip-addr","-V", 1},
                {"--login",      "-s", 1},
                {"--logout",     "-e", 1},
                {"--mac-addr",   "-m", 1},
                {"--uicid",      "-a", 1},
                {"--sort",       "-S", 1},
                {"--help",       "-h", 0},
                {"--verbose",    "-v", 0},
                {NULL, NULL, 0}
        };
        assert( (ctx != NULL) && (ctx->dbc != NULL) && (ctx->dbc->config != NULL));

        eurephiaXML_CreateDoc(ctx, 1, "lastlog_query", &srch_xml, &srch_n);
        fmap_n = xmlNewChild(srch_n, NULL, (xmlChar *) "fieldMapping", NULL);
        xmlNewProp(fmap_n, (xmlChar *) "table", (xmlChar *) "lastlog");

        // Parse arguments
        for( i = 1; i < argc; i++ ) {
                switch( eurephia_getopt(&i, argc, argv, listargs) ) {
                case 'S':
                        sortkeys = optargs[0];
                        break;

                case 'h':
                        help_Lastlog();
                        xmlFreeDoc(srch_xml);
                        return 0;

                case 'c':
                        xmlNewChild(fmap_n, NULL, (xmlChar *) "certid", (xmlChar *) optargs[0]);
                        break;

                case 'i':
                        xmlNewChild(fmap_n, NULL, (xmlChar *) "uid", (xmlChar *) optargs[0]);
                        break;

                case 'u':
                        xmlNewChild(fmap_n, NULL, (xmlChar *) "username", (xmlChar *) optargs[0]);
                        break;

                case 'I':
                        xmlNewChild(fmap_n, NULL, (xmlChar *) "ip", (xmlChar *) optargs[0]);
                        break;

                case 'V':
                        xmlNewChild(fmap_n, NULL, (xmlChar *) "vpnip", (xmlChar *) optargs[0]);
                        break;

                case 's':
                        flt_n = xmlNewChild(fmap_n, NULL, (xmlChar *) "login", (xmlChar *) optargs[0]);
                        xmlNewProp(flt_n, (xmlChar *) "filter", (xmlChar *) "greater-than-equals");
                        break;

                case 'e':
                        flt_n = xmlNewChild(fmap_n, NULL, (xmlChar *) "logout", (xmlChar *) optargs[0]);
                        xmlNewProp(flt_n, (xmlChar *) "filter", (xmlChar *) "less-than-equals");
                        break;

                case 'm':
                        xmlNewChild(fmap_n, NULL, (xmlChar *) "macaddr", (xmlChar *) optargs[0]);
                        break;

                case 'a':
                        xmlNewChild(fmap_n, NULL, (xmlChar *) "uicid", (xmlChar *) optargs[0]);
                        break;

                case 'v':
                        xsltparams[1] = "'details2'";
                        break;

                default:
                        fprintf(stderr, "%s: Invalid argument: %s\n", MODULE, argv[i-1]);
                        xmlFreeDoc(srch_xml);
                        return 1;
                }
        }

        lastlog_xml = eDBadminGetLastlog(ctx, srch_xml, sortkeys);
        xmlFreeDoc(srch_xml);
        if( lastlog_xml == NULL ) {
                fprintf(stderr, "%s: Error retrieving lastlog entries\n", MODULE);
                return 1;
        }

        xslt_print_xmldoc(stdout, cfg, lastlog_xml, "lastlog.xsl", (const char **) xsltparams);

        xmlFreeDoc(lastlog_xml);
        return 0;
}
