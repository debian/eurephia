/* xsltparser.c  --  Generic XSLT parser for eurephiadm
 *
 *  GPLv2 only - Copyright (C) 2009 - 2012
 *               David Sommerseth <dazo@users.sourceforge.net>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; version 2
 *  of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

/**
 * @file   xsltparser.h
 * @author David Sommerseth <dazo@users.sourceforge.net>
 * @date   2009-03-29
 *
 * @brief  Generic XSLT parser for eurephiadm
 *
 */

#ifndef   	XSLTPARSER_H_
# define   	XSLTPARSER_H_

int xslt_print_xmldoc(FILE *dst, eurephiaVALUES *cfg, xmlDoc *xmldoc,
                      const char *xsltfname, const char **xsltparams);

#endif 	    /* !XSLTPARSER_H_ */
