/* get_console_input.c -- function to simplify TTY input
 *
 *  GPLv2 only - Copyright (C) 2008 - 2012
 *               David Sommerseth <dazo@users.sourceforge.net>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; version 2
 *  of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

/**
 * @file   get_console_input.c
 * @author David Sommerseth <dazo@users.sourceforge.net>
 * @date   2008-11-29
 *
 * @brief   Simple function to retrieve user input from the console
 *
 */

#include <stdio.h>
#include <termios.h>
#include <string.h>


/**
 * Prints a prompt to the screen and waits for user input via the tty.
 *
 * @param buf     char* to the buffer where the user input is to be stored.
 * @param len     size of the input buffer
 * @param prompt  String printed before the user is asked for input
 * @param hidden  If set to 1, the user input will not be echoed to the tty,
 *                which is useful for passwords.
 *
 * @return Returns the number of bytes received from the user.  On error -1 is returned
 */
int get_console_input(char *buf, size_t len, const char *prompt, int hidden) {
        struct termios term_orig, term_noecho;
        char *res = NULL;
        char *ptr;

        // Print prompt
        fprintf(stdout, "%s ", prompt);

        if( hidden == 1 ) {
                // Get current terminal settings
                if( tcgetattr(fileno(stdin), &term_orig) ) {
                        return -2; // Could not get the current terminal status
                }
                // Create a copy of current settings and turn off echo to terminal
                memcpy(&term_noecho, &term_orig, sizeof(struct termios));
                term_noecho.c_lflag &= ~ECHO;
                if( tcsetattr(fileno(stdin), TCSAFLUSH, &term_noecho) ) {
                        return -3; // Could not set the new terminal settings
                }
        }

        // Read user input from stdin
        res = fgets(buf, len, stdin);

        if( hidden == 1 ) {
                // Restore terminal to saved state
                tcsetattr(fileno(stdin), TCSANOW, &term_orig);
        }

        // Remove trailing spaces
        if( res != NULL && buf != NULL ) {
                ptr = buf + strlen(buf) - 1;
                while( (ptr > buf) && ((*ptr == 0x20) || (*ptr == '\n') || (*ptr == '\r')) ) {
                        *ptr = 0;
                        ptr--;
                }
                ptr++;
        } else {
                // If nothing is read, make sure result buffer is cleared
                memset(buf, 0, len);
        }
        if( hidden ) {
                fprintf(stdout, "\n");
        }

        return (buf != NULL ? strlen(buf) : -1); 
}
