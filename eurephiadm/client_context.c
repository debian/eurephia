/* client_context.c  --  Handles eurephia contexts used by admin interfaces
 *
 *  GPLv2 only - Copyright (C) 2008 - 2012
 *               David Sommerseth <dazo@users.sourceforge.net>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; version 2
 *  of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
*/

/**
 * @file   client_context.c
 * @author David Sommerseth <dazo@users.sourceforge.net>
 * @date   2008-12-01
 *
 * @brief  Functions for working with eurephiaCTX outside the openvpn plug-in
 *
 */

#include <stdio.h>
#include <string.h>
#include <assert.h>

#include <eurephia_nullsafe.h>
#include <eurephia_context.h>
#include <eurephia_values_struct.h>
#include <eurephia_values.h>
#include <eurephiadb.h>


/**
 * Initialises a new eurephiaCTX.  This function also initialises the database driver, which must
 * be configured in the configuration.
 *
 * @param log      String containing log destination
 * @param logident String used to identify log entries when logging to syslog
 * @param loglevel Set the log level (verbosity)
 * @param cfg      eurephiaVALUES pointer to the configuration
 *
 * @return Returns a pointer to a eurephiaCTX, otherwise NULL.
 */
eurephiaCTX *eurephiaCTX_init(const char *logident, const char *log,
			      const int loglevel, eurephiaVALUES *cfg)
{
        eurephiaCTX *ctx = NULL;
        char *dbdriver = NULL, *logfile = NULL;
        int cfgloglvl = 0;

        dbdriver = eGet_value(cfg, "database_driver");
        if( dbdriver == NULL ) {
                fprintf(stderr, "ERROR: No database driver given in config file (database_driver)\n");
                return NULL;
        }

        ctx = (eurephiaCTX *) malloc_nullsafe(NULL, sizeof(eurephiaCTX)+2);
        assert(ctx != NULL);
        ctx->context_type = ECTX_ADMIN_CONSOLE;

        // Open log file.  Use config file as default if it exists, if not use input param.
        cfgloglvl = ((eGet_value(cfg, "log_level") == NULL)
                     ? loglevel : atoi_nullsafe(eGet_value(cfg, "log_level")));

         logfile = eGet_value(cfg, "log");
        if( (logfile != NULL) && (log == NULL) ) {
                eurephia_log_init(ctx, logident, logfile, (loglevel > 0 ? loglevel : cfgloglvl));
        } else {
                // If log file is not set in config, use input log parameter.  But if
                // no log file is defined even here, use stderr.  If no logging is wanted, it
                // must be defined as none: in the config file.
                eurephia_log_init(ctx, logident, (log != NULL ? log : "stderr:"),
                                  (loglevel > 0 ? loglevel : cfgloglvl));
        }

        if( !eDBlink_init(ctx, dbdriver, 2) ) {
                eurephia_log(ctx, LOG_PANIC, 0, "Could not load the database driver");
                free_nullsafe(ctx, ctx);
                return NULL;
        }
        return ctx;
}


/**
 * Takes care of tearing down a eurephiaCTX in a proper way.  It will disconnect from the 
 * database and unload the database driver if needed.
 *
 * @param ctx eurephiaCTX to take down.
 */
void eurephiaCTX_destroy(eurephiaCTX *ctx) {
        if( ctx == NULL ) {
                return;
        }

        if( (ctx->dbc != NULL) && (ctx->dbc->dbhandle != NULL) ) {
                eDBdisconnect(ctx);
        }

        if( ctx->eurephia_driver != NULL ) {
                eDBlink_close(ctx);
        }

        eurephia_log_close(ctx);
        free_nullsafe(ctx, ctx);
}
