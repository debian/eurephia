/* get_console_input.h  --  Get input from console
 *
 *  GPLv2 only - Copyright (C) 2008 - 2012
 *               David Sommerseth <dazo@users.sourceforge.net>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; version 2
 *  of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

/**
 * @file   get_console_input.h
 * @author David Sommerseth <dazo@users.sourceforge.net>
 * @date   2008-11-29
 *
 * @brief   Simple function to retrieve user input from the console
 *
 */

#ifndef _GETPASSWORD_H
#define _GETPASSWORD_H

int get_console_input(char *buf, size_t len, const char *prompt, int hidden);

#endif
