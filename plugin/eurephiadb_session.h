/* eurephia_session.h  --  Internal API to provide storing of variables connected to a session
 *
 *  GPLv2 only - Copyright (C) 2008 - 2012
 *               David Sommerseth <dazo@users.sourceforge.net>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; version 2
 *  of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

/**
 * @file   eurephiadb_session.h
 * @author David Sommerseth <dazo@users.sourceforge.net>
 * @date   2008-08-06
 *
 * @brief  Handles creating user sessions, which is unique per openvpn connection.
 *
 */

#ifndef   	EUREPHIADB_SESSION_H_
# define   	EUREPHIADB_SESSION_H_

#include <eurephiadb_session_struct.h>

eurephiaSESSION *eDBopen_session_seed(eurephiaCTX *ctx, const char *digest,
                                      const char *cname, const char *username,
                                      const char *vpnipaddr, const char *vpnipmask,
                                      const char *remipaddr, const char *remport);

eurephiaSESSION *eDBsession_load(eurephiaCTX *ctx, const char *sesskey, sessionType sesstype);
eurephiaSESSION *eDBopen_session_macaddr(eurephiaCTX *ctx, const char *macaddr);

#endif 	    /* !EUREPHIADB_SESSION_H_ */
