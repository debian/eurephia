#  cmake rules for eurephia - OpenVPN authentication plugin
#
#  GPLv2 only - Copyright (C) 2008 - 2012
#               David Sommerseth <dazo@users.sourceforge.net>
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; version 2
#  of the License.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
PROJECT(eurephia-auth C)
cmake_minimum_required(VERSION 2.6)

# Check for POSIX semaphore functions which we use
CHECK_LIBRARY_EXISTS(rt sem_wait "" HAVE_SEM_WAIT)
CHECK_LIBRARY_EXISTS(rt sem_timedwait "" HAVE_SEM_TIMEDWAIT)
CHECK_LIBRARY_EXISTS(rt sem_post "" HAVE_SEM_POST)

# Extra checks if sem_* functions is not found in librt, do not accept failures
IF(NOT HAVE_SEM_WAIT)
       message(STATUS "* sem_wait was not found in librt, trying libpthread")
       CHECK_LIBRARY_EXISTS(pthread sem_wait "" HAVE_SEM_WAIT2)
       IF(NOT HAVE_SEM_WAIT2)
              message(FATAL_ERROR "Missing proper pthread semaphore support")
       ENDIF(NOT HAVE_SEM_WAIT2)
ENDIF(NOT HAVE_SEM_WAIT)

IF(NOT HAVE_SEM_TIMEDWAIT)
       message(STATUS "* sem_timedwait was not found in librt, trying libpthread")
       CHECK_LIBRARY_EXISTS(pthread sem_timedwait "" HAVE_SEM_TIMEDWAIT2)
       IF(NOT HAVE_SEM_TIMEDWAIT2)
              message(FATAL_ERROR "Missing proper pthread semaphore support")
       ENDIF(NOT HAVE_SEM_TIMEDWAIT2)
ENDIF(NOT HAVE_SEM_TIMEDWAIT)

IF(NOT HAVE_SEM_POST)
       message(STATUS "* sem_post was not found in librt, trying libpthread")
       CHECK_LIBRARY_EXISTS(pthread sem_post "" HAVE_SEM_POST2)
       IF(NOT HAVE_SEM_POST2)
              message(FATAL_ERROR "Missing proper pthread semaphore support")
       ENDIF(NOT HAVE_SEM_POST2)
ENDIF(NOT HAVE_SEM_POST)

# Check for POSIX MQ functions which we use
CHECK_LIBRARY_EXISTS(rt mq_open "" HAVE_RT_MQ_OPEN)
CHECK_LIBRARY_EXISTS(rt mq_close "" HAVE_RT_MQ_CLOSE)
CHECK_LIBRARY_EXISTS(rt mq_unlink "" HAVE_RT_MQ_UNLINK)
CHECK_LIBRARY_EXISTS(rt mq_send "" HAVE_RT_MQ_SEND)
CHECK_LIBRARY_EXISTS(rt mq_receive "" HAVE_RT_MQ_RECEIVE)
CHECK_LIBRARY_EXISTS(rt mq_getattr "" HAVE_RT_MQ_GETATTR)

# Fail if we're missing some features - Posix Message Queue functions
IF(NOT HAVE_RT_MQ_OPEN OR NOT HAVE_RT_MQ_CLOSE OR NOT HAVE_RT_MQ_UNLINK OR NOT HAVE_RT_MQ_SEND OR NOT HAVE_RT_MQ_RECEIVE OR NOT HAVE_RT_MQ_GETATTR)
	message(FATAL_ERROR "Missing proper pthread message queue support")
ENDIF(NOT HAVE_RT_MQ_OPEN OR NOT HAVE_RT_MQ_CLOSE OR NOT HAVE_RT_MQ_UNLINK OR NOT HAVE_RT_MQ_SEND OR NOT HAVE_RT_MQ_RECEIVE OR NOT HAVE_RT_MQ_GETATTR)


# Compiler settings
INCLUDE_DIRECTORIES(../common ../database ./firewall .)


# Do build in subdirs, if some extra modules are enabled
SET(subdirs "")
IF(FW_IPTABLES)
	# iptables module support
        message(STATUS "Will build iptables firewall module")
       SET(subdirs firewall/iptables)
ENDIF(FW_IPTABLES)


# Avoid adding administration features into the openvpn plug-in
IF(ENABLE_EUREPHIADM)
       REMOVE_DEFINITIONS(-DENABLE_EUREPHIADM)
ENDIF(ENABLE_EUREPHIADM)


# Build rule for eurephia-auth.so
ADD_LIBRARY(eurephia-auth MODULE
	eurephia-auth.c
	eurephia.c
	eurephiadb_session.c
        environment.c
        firewall/eurephiafw.c
        firewall/eurephiafw_helpers.c
        ../common/eurephiadb_session_common.c
)
SET_TARGET_PROPERTIES(eurephia-auth PROPERTIES PREFIX "")

# Link in libraries
TARGET_LINK_LIBRARIES(eurephia-auth pthread rt crypto common)

# Install rules
INSTALL(TARGETS eurephia-auth  LIBRARY DESTINATION ${PLUGINDIR})
INSTALL(FILES eurephia-auth.7 DESTINATION ${MANDIR}/man7)

# If subdirs contains more directories with build/install rules, do that
IF(subdirs)
        ADD_SUBDIRECTORY(${subdirs})
ENDIF(subdirs)
