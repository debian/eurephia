#  cmake rules for eurephia - doxygen documentation
#
#  GPLv2 only - Copyright (C) 2009 - 2012
#               David Sommerseth <dazo@users.sourceforge.net>
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; version 2
#  of the License.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
PROJECT(eurephia-devel-docs C)
cmake_minimum_required(VERSION 2.6)

IF(DOXYGEN)
        FIND_PACKAGE(Doxygen)
        IF(NOT DOXYGEN_FOUND)
               MESSAGE(FATAL_ERROR "Could not find doxygen")
        ENDIF(NOT DOXYGEN_FOUND)
        ADD_CUSTOM_TARGET(devel-docs ALL ${DOXYGEN_EXECUTABLE} Doxygen.conf)
        SET_DIRECTORY_PROPERTIES(PROPERTIES ADDITIONAL_MAKE_CLEAN_FILES doxygen.log)
ENDIF(DOXYGEN)
