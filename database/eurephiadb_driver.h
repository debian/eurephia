/* eurephiadb_driver.h  --  API provided by the database driver
 *
 *  GPLv2 only - Copyright (C) 2008 - 2012
 *               David Sommerseth <dazo@users.sourceforge.net>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; version 2
 *  of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

/**
 * @file   eurephiadb_driver.h
 * @author David Sommerseth <dazo@users.sourceforge.net>
 * @date   2008-08-06
 *
 * @brief  eurephia database driver API.  Functions and constants
 *         provided by and used by the database drivers.
 *
 */

#include <eurephia_driverapi.h>
#include <eurephia_context.h>
#include <eurephiadb_session_struct.h>
#include <eurephia_values_struct.h>

#ifdef HAVE_LIBXML2
#include <libxml/tree.h>
#endif

#ifndef EUREPHIADB_DRIVER_H_
#define EUREPHIADB_DRIVER_H_

#define attempt_IPADDR      1   /**< type code for registering attempts on IP address */
#define attempt_CERTIFICATE 2   /**< type code for registering attempts on certificate */
#define attempt_USERNAME    3   /**< type code for registering attempts on username */

#define ATTEMPT_RESET    0x0A   /**< mode code for resetting attempts count */
#define ATTEMPT_REGISTER 0x0B   /**< mode code for registering a new attempt */

#define USERINFO_user       0x01 /**< flag for extracting user account information */
#define USERINFO_certs      0x02 /**< flag for extracting certificate information */
#define USERINFO_lastlog    0x04 /**< flag for extracting lastlog information */
#define USERINFO_attempts   0x08 /**< flag for extracting information from attempts log */
#define USERINFO_blacklist  0x10 /**< flag for extracting information from blacklist log */

/**
 * Mandatory function. Retrieves driver version information
 *
 * @return Returns string (const char *) containing driver version information.
 */
const char * EUREPHIA_DRIVERAPI_FUNC(eDB_DriverVersion)(void);


/**
 * Mandatory function. Retrieves driver API level
 *
 * @return Returns integer value with API level supported by driver.
 */
int EUREPHIA_DRIVERAPI_FUNC(eDB_DriverAPIVersion)(void);

/*
 *    functions which needs to exists in the API level 1
 */

/**
 * Connect to a database
 *
 * @version API version level 1
 * @param ctx  eurephiaCTX - context to which the database connection will be established against.
 * @param argc number of arguments sent in the argument vector
 * @param argv char** argument vector with driver specific argument for
 *             establishing a database connection
 *
 * @return Returns 1 on success, otherwise 0.
 */
int EUREPHIA_DRIVERAPI_FUNC(eDBconnect)(eurephiaCTX *ctx, const int argc, const char **argv);

/**
 * Disconnects from a database
 *
 * @version API version level 1
 * @param ctx eurephiaCTX - context with the database connection to disconnect from.
 *
 */
void EUREPHIA_DRIVERAPI_FUNC(eDBdisconnect) (eurephiaCTX *ctx);

/**
 * Authenticates a certificate against the database.
 *
 * @version API version level 1
 * @param ctx    eurephiaCTX
 * @param org    X.509 organisation field (O)
 * @param cname  X.509 common name field (CN)
 * @param email  X.509 email field (emailAddress)
 * @param digest Certificate SHA1 fingerprint (digest)
 * @param depth  Certificate depth. 0 is for user certificates. 1 and higher is for CA certificates,
 *               according to the certificates position in the certificate chain.
 *
 * @return Returns certid (certificate ID) on success.  0 is returned if certificate is not found,
 *         or -1 if the certificate is blacklisted.
 */
int EUREPHIA_DRIVERAPI_FUNC(eDBauth_TLS)(eurephiaCTX *ctx, const char *org, const char *cname,
					 const char *email,
					 const char *digest, const unsigned int depth);

/**
 * Authenticates a client against the database, with users certificate ID, username and password.
 *
 * @version API version level 1
 * @param ctx eurephiaCTX
 * @param certid certificate ID to the user being authenticated
 * @param username username to be authenticated
 * @param passwd password provided by the user
 *
 * @return Returns uicid (user-certs ID) to the user on success.  0 if user account is not
 *         found and -1 on authentication failure.
 */
int EUREPHIA_DRIVERAPI_FUNC(eDBauth_user)(eurephiaCTX *ctx, const int certid,
					  const char *username, const char *passwd);


/**
 * Retrieve the user ID (uid) for a given user and certificate.
 *
 * @version API version level 1
 * @param ctx eurephiaCTX
 * @param certid Certificate ID of the user
 * @param username username of the user
 *
 * @return Returns uid of user on success, 0 if user account is not found, otherwise -1 on errors.
 */
int EUREPHIA_DRIVERAPI_FUNC(eDBget_uid)(eurephiaCTX *ctx, const int certid, const char *username);


/**
 * Checks if a user account (attempt_USERNAME), certificate (attempt_CERTIFICATE) or
 * IP address (attempt_IPADDR) is blacklisted.
 *
 * @version API version level 1
 * @param ctx eurephiaCTX
 * @param type Must be one of the constants: attempt_USERNAME, attempt_CERTIFICATE or attempt_IPADDR
 * @param val  Value to be checked against the blacklist.
 *
 * @return Returns 1 if a matching record was found in the blacklist table.  Otherwise 0 is returned.
 *
 * @see attempt_IPADDR, attempt_CERTIFICATE, attempt_USERNAME
 */
int EUREPHIA_DRIVERAPI_FUNC(eDBblacklist_check)(eurephiaCTX *ctx, const int type, const char *val);


/**
 * Registers an attempt in the attempts log.  If the number of attempts exceeds the
 * configured attempts limit, it will also be blacklisted immediately.
 *
 * @version API version level 1
 * @param ctx eurephiaCTX
 * @param type Must be one of the constants: attempt_USERNAME, attempt_CERTIFICATE or attempt_IPADDR
 * @param mode Must be one of the constants: ATTEMPT_RESET to reset the attempts count
 *             or ATTEMPT_REGISTER to register an attempt.
 * @param value Value of the what to be registered.
 *
 * @see ATTEMPT_REGISTER, ATTEMPT_RESET, attempt_IPADDR, attempt_CERTIFICATE, attempt_USERNAME
 */
void EUREPHIA_DRIVERAPI_FUNC(eDBregister_attempt)(eurephiaCTX *ctx, int type, int mode, const char *value);


/**
 * Registers a client login.  This happens after the client has been authenticated successfully,
 * when OpenVPN does the OPENVPN_PLUGIN_CLIENT_CONNECT call to the eurephia-auth plug-in.
 *
 * @version API version level 1
 * @param ctx        eurephiaCTX
 * @param skey       eurephiaSESSION.  A login must be connected to an opened eurephia session.
 * @param certid     Certificate ID of the clients user certificate
 * @param uid        User id of the client
 * @param proto      String containing protocol used for the connection (udp, tcp)
 * @param remipaddr  Clients remote IP address
 * @param remport    The port the client is connecting from
 * @param vpnipaddr  The IP address openvpn assigned to the user
 * @param vpnipmask  The VPN networks netmask for the VPN connection.
 *
 * @return Returns 1 on success, otherwise 0.
 */
int EUREPHIA_DRIVERAPI_FUNC(eDBregister_login)(eurephiaCTX *ctx, eurephiaSESSION *skey,
					       const int certid, const int uid,
					       const char *proto, const char *remipaddr, const char *remport,
					       const char *vpnipaddr, const char *vpnipmask);


/**
 *  Registers the MAC address of the clients TAP interface.  This function is called when
 *  OpenVPN does the OPENVPN_PLUGIN_LEARN_ADDRESS call to the eurephia-auth plug-in.
 *
 * @version API version level 1
 * @param ctx     eurephiaCTX
 * @param session eurephiaSESSION of the user
 * @param macaddr String (char *) containing the MAC address of the clients interface.
 *
 * @return Returns 1 on success, otherwise 0.
 */
int EUREPHIA_DRIVERAPI_FUNC(eDBregister_vpnmacaddr)(eurephiaCTX *ctx, eurephiaSESSION *session,
						    const char *macaddr);


/**
 *  Registers the VPN clients address.  This function is called when
 *  OpenVPN does the OPENVPN_PLUGIN_LEARN_ADDRESS call to the eurephia-auth plug-in.
 *  In TAP mode the clients VPN MAC and IP address are stored, in TUN mode the VPN IP
 *  address is stored.
 *
 * @version API version level 3
 * @param ctx        eurephiaCTX
 * @param session    eurephiaSESSION of the user
 * @param macaddr    String (char *) containing the MAC address of the clients interface.
 * @param vpnip4addr String (char *) containing the IPv4 address of the clients interface.
 * @param vpnip6addr String (char *) containing the IPv6 address of the clients interface.
 *
 * @return Returns 1 on success, otherwise 0.
 */
int EUREPHIA_DRIVERAPI_FUNC(eDBregister_vpnclientaddr)(eurephiaCTX *ctx, eurephiaSESSION *session,
                                                       const char * macaddr,
                                                       const char * vpnip4addr, const char *vpnip6addr);

/**
 * Registers when a user logged out.  It will then add some information about the session to the
 * eurephia lastlog.
 *
 * @version API version level 1
 * @param ctx            eurephiaCTX
 * @param skey           eurephiaSESSOIN of the user
 * @param bytes_sent     Amount of bytes the OpenVPN server sent to the client
 * @param bytes_received Amount of bytes the OpenVPN server received from the client
 * @param duration       How long the session lasted (in seconds)
 *
 * @return Returns 1 on success, otherwise 0.
 */
int EUREPHIA_DRIVERAPI_FUNC(eDBregister_logout)(eurephiaCTX *ctx, eurephiaSESSION *skey,
						const char *bytes_sent, const char *bytes_received,
						const char *duration);


/* firewall functions */
/**
 * Retrieves the name of the firewall profile the user access (user account + certificate)
 * for the user session
 *
 * @version API version level 1
 * @param ctx eurephiaCTX
 * @param session eurephiaSESSION
 *
 * @return Returns a char pointer to a buffer with the name of the firewall profile. This buffer
 *         must be freed when no longer needed.
 */
char * EUREPHIA_DRIVERAPI_FUNC(eDBget_firewall_profile) (eurephiaCTX *ctx, eurephiaSESSION *session);

/**
 * Retrieve a list of IP addresses found in the IP address blacklist table.
 *
 * @version API version level 1
 * @param ctx eurephiaCTX
 *
 * @return Returns an eurephiaVALUES chain with all blacklisted IP addresses on success, otherwise
 *         NULL is returned
 */
eurephiaVALUES * EUREPHIA_DRIVERAPI_FUNC(eDBget_blacklisted_ip)(eurephiaCTX *ctx);

/* The following functions is also declared in eurephia_session_values.c - for local internal usage. */
/**
 * Retrieve a unique session key based on a session seed.
 *
 * @version API version level 1
 * @param ctx         eurephiaCTX
 * @param type        Must be either stSESSION for a normal session or stAUTHENTICATION for an
 *                    authentication session (before the user is really logged in)
 * @param sessionseed session seed of the current connection
 *
 * @return Returns the unique session key string (char *) on success, otherwise NULL.  The session key
 *         string must be freed when no longer needed.
 */
char * EUREPHIA_DRIVERAPI_FUNC(eDBget_sessionkey_seed)(eurephiaCTX *ctx, sessionType type,
						      const char *sessionseed);


/**
 * Retrieve a unique session key based on a connections MAC address.  This is called when
 * OpenVPN is removing the MAC address of the client as a known connection.
 *
 * @version API version level 1
 * @param ctx      eurephiaCTX
 * @param macaddr  String (char *) containing the MAC address of the client
 *
 * @return Returns the unique session key string (char *) on success, otherwise NULL.  The session key
 *         string must be freed when no longer needed.
 */
char * EUREPHIA_DRIVERAPI_FUNC(eDBget_sessionkey_macaddr)(eurephiaCTX *ctx, const char *macaddr);


/**
 * Check if a session key is unique.  In other words, it will check the given session key
 * against the database to see if it is found there or not.  If not, it is unique.
 *
 * @version API version level 1
 * @param ctx eurephiaCTX
 * @param seskey String containing the session key
 *
 * @return Returns 1 if the session key is unique and not been used earlier.
 */
int EUREPHIA_DRIVERAPI_FUNC(eDBcheck_sessionkey_uniqueness)(eurephiaCTX *ctx, const char *seskey);


/**
 * Registers a new session key against a short-term session seed.
 *
 * @version API version level 1
 * @param ctx     eurephiaCTX
 * @param seed    A string (char *) containing the short-term session seed
 * @param seskey  A string (char *) containing the new unique session key
 *
 * @return Returns 1 on success, otherwise 0
 */
int EUREPHIA_DRIVERAPI_FUNC(eDBregister_sessionkey)(eurephiaCTX *ctx, const char *seed, const char *seskey);


/**
 * Loads all session variables for a specified session key.  This is key/value pairs
 * which are unique for each connection.
 *
 * @version API version level 1
 * @param ctx     eurephiaCTX
 * @param sesskey String (char *) containing a session key
 *
 * @return Returns a pointer to an eurephiaVALUES pointer chain with all the variables available
 *         for the given session.  On errors, it will return an empty eurephiaVALUES chain.
 */
eurephiaVALUES * EUREPHIA_DRIVERAPI_FUNC(eDBload_sessiondata)(eurephiaCTX *ctx, const char *sesskey);


/**
 * Destroys a session.  It will remove all stored session variables and mark the session
 * as closed in the lastlog.  It will also remove the session seed/session key reference.
 *
 * @version API version level 1
 * @param ctx      eurephiaCTX
 * @param session  eurephiaSESSION pointer to session to be destroyed
 *
 * @return Returns 1 on success, otherwise 0.
 */
int EUREPHIA_DRIVERAPI_FUNC(eDBdestroy_session)(eurephiaCTX *ctx, eurephiaSESSION *session);


/**
 * Add, update or remove a session variable from the database.  This operation is
 * only affecting the given session.  This function will only update the database itself.
 *
 * @version API version level 1
 * @param ctx      eurephiaCTX
 * @param session  eurephiaSESSION to which the variable will be added, modified or deleted
 * @param mode     Must be one of the constants: SESSVAL_NEW, SESSVAL_UPDATE, SESSVAL_DELETE
 * @param key      Key name for the value to be stored
 * @param val      Value to be stored
 *
 * @return Returns 1 on success, otherwise 0
 * @see SESSVAL_NEW, SESSVAL_UPDATE, SESSVAL_DELETE
 */
int EUREPHIA_DRIVERAPI_FUNC(eDBstore_session_value)(eurephiaCTX *ctx, eurephiaSESSION *session, int mode,
						    const char *key, const char *val);


/*
 *    functions which needs to exists in the API level 2
 */
#ifdef HAVE_LIBXML2
/**
 * Authenticate users and sessions for the administration interface.  The OpenVPN plug-in
 * should never use this API.
 *
 * @version API version level 2
 * @param ctx     eurephiaCTX
 * @param qryxml  eurephia XML document describing the operation to be done
 *
 * XML document describing authentication of a user account
 * @code
 * <eurephia format="1">
 *    <Authenticate mode="user">
 *       <username>{username}</username>
 *       <password>{password}</password>
 *       <accesslevel>{accesslevel}</password>
 *    </Authenticate>
 * </eurephia>
 * @endcode
 *
 * XML document for authenticating and validating a user session to a specific access level
 * @code
 * <eurephia format="1">
 *    <Authenticate mode="session">
 *       <sessionkey>{session key}</sessionkey>
 *       <accesslevel>{accesslevel}</password>
 *    </Authenticate>
 * </eurephia>
 * @endcode
 *
 * XML docuument to register the user as logged in
 * @code
 * <eurephia format="1">
 *    <Register mode="login" uid="{uid}">{session key}</Register>
 * </eurephia>
 * @endcode
 *
 * XML docuument to register the user as logged out
 * @code
 * <eurephia format="1">
 *    <Register mode="logout">{session key}</Register>
 * </eurephia>
 * @endcode
 *
 * @return Returns a valid eurephia ResultMsg XML document with the result.  On fatal errors,
 *         NULL is returned.  When a username/password authentication is done, the user id of the
 *         user will be returned in the details part of the ResultMsg.
 *
 */
xmlDoc * EUREPHIA_DRIVERAPI_FUNC(eDBadminAuthenticate)(eurephiaCTX *ctx, xmlDoc *qryxml);


/**
 * Set or delete configuration parameters in the database.  This operation will
 * also update the in-memory copy of the configuration
 *
 * @param ctx    eurephiaCTX
 * @param cfgxml XML document specifying the operation
 *
 * XML format skeleton for setting or deleting configuration parameters
 * @code
 * <eurephia format="1">
 *      <configuration>
 *         <set key="{name of the key}">{Value to be assigned}</set>
 *         <delete key="{name of the key"}/>
 *      </configuration>
 * </eurephia>
 * @endcode
 * Only one operation can be called per request to this function.
 *
 * @return Returns a valid XML document with information about the operation, or NULL on fatal errors.
 * @see eurephiaXML_CreateDoc()
 */
xmlDoc * EUREPHIA_DRIVERAPI_FUNC(eDBadminConfiguration)(eurephiaCTX *ctx, xmlDoc *cfgxml);


/**
 * Function for view, add, update or delete user accounts
 *
 * @param ctx eurephiaCTX
 * @param qryxml XML document with information about the operation
 *
 * @code
 *   <eurephia format="1">
 *      <UserAccount mode="{view|add|update|delete}" [uid="{uid}"]>
 *         <fieldMapping table="users">
 *             <{field name}>{field value}</{field name}>
 *             ...
 *             <{field name}>{field value}</{field name}>
 *         </fieldMapping>
 *        [<sortkeys>{field name}[, {field name}...]</sortkeys>]
 *        [<extractFlags>{extract value (int)}</extractFlags>]
 *      </UserAccount>
 *   </eurephia>
 * @endcode
 * Valid field names are: uid, username, password, activated, deactivated, lastaccess
 *
 * If no field names and values are given in "view" mode, all user accounts will be returned.  Adding
 * field name tags will narrow down the query.
 *
 * For "add" mode, username and password tag is required. For the password tag, the attribute "pwhash"
 * can be given to indicate what kind of hashing the value provided is using.  If this is not set, the
 * password value is expected to come in clear text and will be hashed automatically with a SHA512
 * algorithm.
 *
 * For the "update" mode, the uid attribute in the UserAccount tag is required.  This will be the
 * key for which record to update.  The values being updated need to be set in the fieldMapping tags.
 *
 * For "delete" mode, the "uid" attribute in the UserAccount tag is required.  This mode will also ignore
 * any fieldMapping tags.
 *
 * @return When mode is "view", it will return an XML document with user account information on success.
 *         On errors in "view" mode or the other modes in general, it will return an eurephia ResultMsg
 *         XML document with the result of the operation, or NULL on fatal errors.
 *
 * @see eurephiaXML_CreateDoc(), eurephiaXML_ParseResultMsg(), eurephiaXML_getRoot()
 */
xmlDoc * EUREPHIA_DRIVERAPI_FUNC(eDBadminUserAccount)(eurephiaCTX *ctx, xmlDoc *qryxml);


/**
 * Function for listing, registering new or deleting certificates in the database
 *
 * @version API version level 2
 * @param ctx eurephiaCTX
 * @param qryxml XML document with information about the operation
 *
 * Skeleton of an XML document used for eDBadminCertificate()
 * @code
 *    <eurephia format="1">
 *      <certificates mode="{list|register|delete}">
 *        <fieldMapping table="certificates">
 *            <{field name}>{field value}</{field name}>
 *            ...
 *            <{field name}>{field value}</{field name}>
 *        </fieldMapping>
 *        [<sortkeys>{field name}[, {field name}...]</sortkeys>]
 *      </certificates>
 *    </eurephia>
 * @endcode
 * Valid field names are: depth, digest, common_name, org, email and certid.
 * For list and delete mode, all field names can be used to narrow the search query.
 * In register mode all fields are required, except certid which must not be given.
 *
 * The sortkeys tag will only be used in list mode, and it takes a list of comma separated
 * field names.
 *
 * @return When mode is "list", it will return an XML document with all found certificates, otherwise NULL.
 *         If mode is "register" or "delete" an eurephia ResultMsg XML document will be returned with
 *         the result of the operation, or NULL on fatal errors.
 *
 * Skeleton of a result document from a "list" mode query.
 * @code
 * <eurephia format="1">
 *    <certificates certificates={number of found certs}">
 *        <certificate certid="{int}" depth="{int}" registered="{date+time}">
 *            <digest>{SHA1 digest of cert}</digest>
 *            <common_name>{(CN) common name of cert}</common_name>
 *            <organisation>{(O) organisation name of cert}</organisation>
 *            <email>{(emailAddr) e-mail address found in cert}</email>
 *        </certificate>
 *    </certificates>
 * </eurephia>
 * @endcode
 *
 * @see eurephiaXML_CreateDoc(), eurephiaXML_ParseResultMsg(), eurephiaXML_getRoot()
 */
xmlDoc * EUREPHIA_DRIVERAPI_FUNC(eDBadminCertificate)(eurephiaCTX *ctx, xmlDoc *qryxml);


/**
 * Retrieve, add, modify or delete user/certificate links
 *
 * @version API version level 2
 * @param ctx eurephiaCTX
 * @param usrcrt_xml XML document containing operation information
 *
 * Skeleton of needed XML document
 * @code
 *    <eurephia format="1">
 *        <usercerts mode="{search|register|remove|update}" [uicid="{uicid}"]>
 *            <fieldMapping table="usercerts">
 *               <{field name}>{search value}</{field name}>
 *            </fieldMapping>
 *            [<sortfields>{field name}[, {field name},...]</sortfields>] <!-- Only for mode='search' -->
 *        </usercerts
 *    </eurephia>
 * @endcode
 *
 * In search mode, it can be several field name tags to narrow the search.
 * If mode is 'update' the 'uicid' attribute must be present in the usercerts tag.
 *
 * @return Returns a valid eurephia XML document on success.  If errors happened, either an XML document
 *         with an error message will be returned or NULL.
 * @see eurephiaXML_CreateDoc(), eurephiaXML_getRoot()
 */
xmlDoc * EUREPHIA_DRIVERAPI_FUNC(eDBadminUserCertsLink)(eurephiaCTX *ctx, xmlDoc *usrcrt_xml);


/**
 * List, grant or revoke access to the administration utilities
 *
 * @version API version level 2
 * @param ctx eurephiaCTX
 * @param qryxml XML document with the operation and information.
 *
 * XML format skeleton for grant_xml
 * @code
 * <eurephia format="1">
 *      <admin_access mode="{list|grant|revoke}">
 *          <fieldMapping table="eurephia_adminaccess">
 *              <uid>{user id}</uid>
 *              <interface>{C|W}</uid>
 *              <accesslevel>{access level string}</accesslevel>
 *          </fieldMapping>
 *      </admin_access>
 * </eurephia>
 * @endcode
 * To grant or revoke access, all fields are needed.  For list mode, any given fields will narrow
 * the database query.  If no fields are given to list mode, all registered records will be returned.
 * The interface tag can have two valid values, C for console interface and W for web interface.
 *
 * XML format used for list mode:
 * @code
 * <eurephia format="1">
 *     <admin_access_list>
 *         <user_access>
 *             <username uid="{uid}">{username}</username>
 *             <access_levels>
 *                 <access interface="{interface}">{access level string}</access>
 *                 ... (more access tags)
 *                 ...
 *             </access_levels>
 *         </user_access>
 *         ... (another user_access tag)
 *     </admin_access_list>
 * </eurephia>
 * @endcode
 *
 * @return Returns a valid eurephia XML document with the result of success or failure.
 *         On fatal errors NULL is returned.
 * @see eurephiaXML_CreateDoc(), eurephiaXML_getRoot()
 */
xmlDoc * EUREPHIA_DRIVERAPI_FUNC(eDBadminAccessLevel)(eurephiaCTX *ctx, xmlDoc *qryxml);


/**
 * Query or modify the firewall access profiles.
 *
 * @version API version level 2
 * @param ctx eurephiaCTX
 * @param xmlqry XML document with the operation and information.
 *
 * The XML format skeleton
 * @code
 *    <eurephia format="1">
 *        <firewall_profiles mode="{search|add|delete}">
 *            <fieldMapping table="fwprofiles">
 *               <{field name}>{search value}</{field name}>
 *            </fieldMapping>
 *        </firewall_profiles>
 *    </eurephia>
 * @endcode
 * It can be several field name tags to narrow the search even more.
 * For the add mode, the fw_profile field name tag must be present.
 *
 * @return Returns a valid XML document with the result on success, otherwise either
 *         NULL or an XML error document is returned.
 * @see eurephiaXML_CreateDoc(), eurephiaXML_getRoot()
 */
xmlDoc * EUREPHIA_DRIVERAPI_FUNC(eDBadminFirewallProfiles)(eurephiaCTX *ctx, xmlDoc *xmlqry);


/**
 * Retrieve the eurephia lastlog
 *
 * @version API version level 2
 * @param ctx eurephiaCTX
 * @param srch_xml XML document specifying the search query
 * @param sortkeys String containing the sort order of the fields
 *
 * The XML format skeleton
 * @code
 *    <eurephia format="1">
 *        <lastlog_query>
 *            <fieldMapping table="lastlog">
 *               <{search field}>{search value}</{search field}>
 *            </fieldMapping>
 *        </lastlog_query>
 *    </eurephia>
 * @endcode
 * Valid field names are: uid, certid, ip, vpnip, status, login, logout, id, username, macaddr, uicid
 *
 * @return Returns a valid XML document on success, otherwise NULL
 * @see eurephiaXML_CreateDoc(), eurephiaXML_getRoot()
 */
xmlDoc * EUREPHIA_DRIVERAPI_FUNC(eDBadminGetLastlog)(eurephiaCTX *ctx, xmlDoc *srch_xml,
						     const char *sortkeys);


/**
 * Query or modify the attempts log.
 *
 * @version API version level 2
 * @param ctx eurephiaCTX
 * @param qryxml document specifying the operation
 *
 * The XML format skeleton
 * @code
 *    <eurephia format="1">
 *        <attemptslog mode="{search|reset|delete}">
 *            <fieldMapping table="attempts">
 *               <{field name}>{field value}</{field field}>
 *            </fieldMapping>
 *        </attemptslog>
 *    </eurephia>
 * @endcode
 * Valid fields are: username, ip, digest, attempts, registered, lastattempt, id
 *
 * @return Returns a valid XML document on success, otherwise NULL or an XML document
 *         with an error message.
 * @see eurephiaXML_CreateDoc(), eurephiaXML_getRoot()
 */
xmlDoc * EUREPHIA_DRIVERAPI_FUNC(eDBadminAttemptsLog)(eurephiaCTX *ctx, xmlDoc *qryxml);


/**
 * Query or modify the attempts log.
 *
 * @version API version level 2
 * @param ctx eurephiaCTX
 * @param qryxml document specifying the operation
 *
 * The XML format skeleton
 * @code
 *    <eurephia format="1">
 *        <blacklist mode="{list|add|delete}">
 *            <fieldMapping table="blacklist">
 *               <{field name}>{field value}</{field field}>
 *            </fieldMapping>
 *        </blacklist>
 *    </eurephia>
 * @endcode
 * For listing and delete mode it be several field tags to limit the search even more.
 * For add mode only username, IP address or certificate digest can be given.
 *
 * @return Returns a valid XML document on success, otherwise NULL or an XML document
 *         with an error message.
 * @see eurephiaXML_CreateDoc(), eurephiaXML_getRoot()
 */
xmlDoc * EUREPHIA_DRIVERAPI_FUNC(eDBadminBlacklist)(eurephiaCTX *ctx, xmlDoc *qryxml);
#endif /* HAVE_LIBXML2 */

#endif /* !EUREPHIADB_DRIVER_H_ */
