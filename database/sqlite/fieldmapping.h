/* fieldmapping.h  --  Contains mapping from internal eurephia field ID to
 *                     SQLite3 specific field names per table
 *
 *  GPLv2 only - Copyright (C) 2008 - 2012
 *               David Sommerseth <dazo@users.sourceforge.net>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; version 2
 *  of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

/**
 * @file   fieldmapping.h
 * @author David Sommerseth <dazo@users.sourceforge.net>
 * @date   2008-12-06
 *
 * @brief  Maps fields names from the generic field names into the ones
 *         being used in the SQLite3 database
 *
 */


#ifndef   	FIELDMAPPING_H_
# define   	FIELDMAPPING_H_

#ifdef FMAP_USERS
static eDBfieldMap tbl_sqlite_users[] = {
        {TABLE_USERS, NULL, FIELD_RECID,       ft_INT     , flt_NOTSET,  "uid",           NULL, NULL},
        {TABLE_USERS, NULL, FIELD_UNAME,       ft_STRING  , flt_NOTSET,  "username",      NULL, NULL},
        {TABLE_USERS, NULL, FIELD_PASSWD,      ft_PASSWD  , flt_NOTSET,  "password",      NULL, NULL},
        {TABLE_USERS, NULL, FIELD_ACTIVATED,   ft_DATETIME, flt_NOTSET,  "activated",     NULL, NULL},
        {TABLE_USERS, NULL, FIELD_DEACTIVATED, ft_DATETIME, flt_NOTSET,  "deactivated",   NULL, NULL},
        {TABLE_USERS, NULL, FIELD_LASTACCESS,  ft_DATETIME, flt_NOTSET,  "last_accessed", NULL, NULL},
        {0, NULL, FIELD_NONE, ft_UNDEF, flt_NOTSET, NULL, NULL, NULL}
};
#endif

#ifdef FMAP_CERTS
static eDBfieldMap tbl_sqlite_certs[] = {
        {TABLE_CERTS, NULL, FIELD_RECID,       ft_INT         , flt_NOTSET, "certid",        NULL, NULL},
        {TABLE_CERTS, NULL, FIELD_CERTDEPTH,   ft_INT         , flt_NOTSET, "depth",         NULL, NULL},
        {TABLE_CERTS, NULL, FIELD_CERTDIGEST,  ft_STRING_LOWER, flt_NOTSET, "digest",        NULL, NULL},
        {TABLE_CERTS, NULL, FIELD_CNAME,       ft_STRING      , flt_NOTSET, "common_name",   NULL, NULL},
        {TABLE_CERTS, NULL, FIELD_ORG,         ft_STRING      , flt_NOTSET, "organisation",  NULL, NULL},
        {TABLE_CERTS, NULL, FIELD_EMAIL,       ft_STRING      , flt_NOTSET, "email",         NULL, NULL},
        {TABLE_CERTS, NULL, FIELD_REGISTERED,  ft_DATETIME    , flt_NOTSET, "registered",    NULL, NULL},
        {0, NULL, FIELD_NONE, ft_UNDEF, flt_NOTSET, NULL, NULL, NULL}
};
#endif

#ifdef FMAP_USERCERTS
static eDBfieldMap tbl_sqlite_usercerts[] = {
        {TABLE_USERCERTS, NULL, FIELD_UID,        ft_INT, flt_NOTSET,  "uid",            NULL, NULL},
        {TABLE_USERCERTS, NULL, FIELD_CERTID,     ft_INT, flt_NOTSET,  "certid",         NULL, NULL},
        {TABLE_USERCERTS, NULL, FIELD_ACCPROFILE, ft_INT, flt_NOTSET,  "accessprofile",  NULL, NULL},
        {TABLE_USERCERTS, NULL, FIELD_REGISTERED, ft_INT, flt_NOTSET,  "registered",     NULL, NULL},
        {TABLE_USERCERTS, NULL, FIELD_RECID,      ft_INT, flt_NOTSET,  "uicid",          NULL, NULL},
        {0, NULL, FIELD_NONE, ft_UNDEF, flt_NOTSET, NULL, NULL, NULL}
};
#endif

#ifdef FMAP_ADMINACCESS
static eDBfieldMap tbl_sqlite_eurephiaadmacc[] = {
        {TABLE_EUREPHIAADMACC, NULL, FIELD_UID,       ft_INT   , flt_NOTSET, "uid",           NULL, NULL},
        {TABLE_EUREPHIAADMACC, NULL, FIELD_INTERFACE, ft_STRING, flt_NOTSET, "interface",     NULL, NULL},
        {TABLE_EUREPHIAADMACC, NULL, FIELD_ACCESSLVL, ft_STRING, flt_NOTSET, "access",        NULL, NULL},
        {0, NULL, FIELD_NONE, ft_UNDEF, flt_NOTSET, NULL, NULL, NULL}
};
#endif

#ifdef FMAP_LASTLOG
static eDBfieldMap tbl_sqlite_lastlog[] = {
        {TABLE_LASTLOG, NULL, FIELD_UID,         ft_INT     , flt_NOTSET,  "uid",           NULL, NULL},
        {TABLE_LASTLOG, NULL, FIELD_CERTID,      ft_INT     , flt_NOTSET,  "certid",        NULL, NULL},
        {TABLE_LASTLOG, NULL, FIELD_REMOTEIP,    ft_STRING  , flt_NOTSET,  "remotehost",    NULL, NULL},
        {TABLE_LASTLOG, NULL, FIELD_VPNIP,       ft_STRING  , flt_NOTSET,  "vpnipaddr",     NULL, NULL},
        {TABLE_LASTLOG, NULL, FIELD_SESSTATUS,   ft_STRING  , flt_NOTSET,  "sessionstatus", NULL, NULL},
        {TABLE_LASTLOG, NULL, FIELD_LOGIN,       ft_DATETIME, flt_NOTSET,  "login" ,        NULL, NULL},
        {TABLE_LASTLOG, NULL, FIELD_LOGOUT,      ft_DATETIME, flt_NOTSET,  "logout",        NULL, NULL},
        {TABLE_LASTLOG, NULL, FIELD_RECID,       ft_INT     , flt_NOTSET,  "llid",          NULL, NULL},
        {TABLE_LASTLOG, NULL, FIELD_UNAME,       ft_STRING  , flt_NOTSET,  "username",      NULL, NULL},
        {TABLE_LASTLOG, NULL, FIELD_MACADDR,     ft_STRING  , flt_NOTSET,  "macaddr",       NULL, NULL},
        {TABLE_LASTLOG, NULL, FIELD_UICID,       ft_STRING  , flt_NOTSET,  "uicid",         NULL, NULL},
        {0, NULL, FIELD_NONE, ft_UNDEF, flt_NOTSET, NULL, NULL, NULL}
};
#endif

#ifdef FMAP_OVPNACCESSES
static eDBfieldMap tbl_sqlite_openvpnaccesses[] = {
        {TABLE_FWPROFILES,   NULL,     FIELD_DESCR,      ft_STRING      , flt_NOTSET,  "access_descr",  NULL, NULL},
        {TABLE_FWPROFILES,   NULL,     FIELD_FWPROFILE,  ft_STRING      , flt_NOTSET,  "fw_profile",    NULL, NULL},
        {TABLE_FWPROFILES,   NULL,     FIELD_RECID,      ft_INT         , flt_NOTSET,  "accessprofile", NULL, NULL},
        {TABLE_FWPROFILES,   NULL,     FIELD_UID,        ft_INT         , flt_NOTSET,  "uid",           NULL, NULL},
        {TABLE_FWPROFILES,   NULL,     FIELD_UNAME,      ft_STRING      , flt_NOTSET,  "username",      NULL, NULL},
        {TABLE_FWPROFILES,   NULL,     FIELD_CERTID,     ft_INT         , flt_NOTSET,  "certid",        NULL, NULL},
        {TABLE_FWPROFILES,   NULL,     FIELD_CNAME,      ft_STRING      , flt_NOTSET,  "common_name",   NULL, NULL},
        {TABLE_FWPROFILES,   NULL,     FIELD_ORG,        ft_STRING      , flt_NOTSET,  "organisation",  NULL, NULL},
        {TABLE_FWPROFILES,   NULL,     FIELD_EMAIL,      ft_STRING      , flt_NOTSET,  "email",         NULL, NULL},
        {TABLE_FWPROFILES,   NULL,     FIELD_CERTDIGEST, ft_STRING_LOWER, flt_NOTSET,  "digest",        NULL, NULL},
        {TABLE_FWPROFILES,   NULL,     FIELD_REGISTERED, ft_DATETIME    , flt_NOTSET,  "registered",    NULL, NULL},
        {0, NULL, FIELD_NONE, ft_UNDEF, flt_NOTSET, NULL, NULL, NULL}
};
#endif

#ifdef FMAP_OVPNATTEMPTS
static eDBfieldMap tbl_sqlite_attempts[] = {
        {TABLE_ATTEMPTS, NULL, FIELD_UNAME,       ft_STRING      , flt_NOTSET, "username",     NULL, NULL},
        {TABLE_ATTEMPTS, NULL, FIELD_REMOTEIP,    ft_STRING      , flt_NOTSET, "remoteip",     NULL, NULL},
        {TABLE_ATTEMPTS, NULL, FIELD_CERTDIGEST,  ft_STRING_LOWER, flt_NOTSET, "digest",       NULL, NULL},
        {TABLE_ATTEMPTS, NULL, FIELD_ATTEMPTS,    ft_INT         , flt_NOTSET, "attempts",     NULL, NULL},
        {TABLE_ATTEMPTS, NULL, FIELD_REGISTERED,  ft_DATETIME    , flt_NOTSET, "registered",   NULL, NULL},
        {TABLE_ATTEMPTS, NULL, FIELD_LASTATTEMPT, ft_DATETIME    , flt_NOTSET, "last_attempt", NULL, NULL},
        {TABLE_ATTEMPTS, NULL, FIELD_RECID,       ft_INT         , flt_NOTSET, "atpid",        NULL, NULL },
        {0, NULL, FIELD_NONE, ft_UNDEF, flt_NOTSET, NULL, NULL, NULL}
};
#endif

#ifdef FMAP_OVPNBLACKLIST
static eDBfieldMap tbl_sqlite_blacklist[] = {
        {TABLE_BLACKLIST, NULL, FIELD_UNAME,       ft_STRING      , flt_NOTSET, "username",      NULL, NULL},
        {TABLE_BLACKLIST, NULL, FIELD_REMOTEIP,    ft_STRING      , flt_NOTSET, "remoteip",      NULL, NULL},
        {TABLE_BLACKLIST, NULL, FIELD_CERTDIGEST,  ft_STRING_LOWER, flt_NOTSET, "digest",        NULL, NULL},
        {TABLE_BLACKLIST, NULL, FIELD_REGISTERED,  ft_DATETIME    , flt_NOTSET, "registered",    NULL, NULL},
        {TABLE_BLACKLIST, NULL, FIELD_LASTACCESS,  ft_DATETIME    , flt_NOTSET, "last_accessed", NULL, NULL},
        {TABLE_BLACKLIST, NULL, FIELD_RECID,       ft_INT         , flt_NOTSET, "blid",          NULL, NULL},
        {0, NULL, 0, ft_UNDEF, flt_NOTSET, NULL, NULL, NULL}
};
#endif

#endif 	    /* !FIELDMAPPING_H_ */
