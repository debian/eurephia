<?xml version="1.0"?>
<!--
     *
     *  GPLv2 only - Copyright (C) 2008 - 2012
     *               David Sommerseth <dazo@users.sourceforge.net>
     *
     *  This program is free software; you can redistribute it and/or
     *  modify it under the terms of the GNU General Public License
     *  as published by the Free Software Foundation; version 2
     *  of the License.
     *
     *  This program is distributed in the hope that it will be useful,
     *  but WITHOUT ANY WARRANTY; without even the implied warranty of
     *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     *  GNU General Public License for more details.
     *
     *  You should have received a copy of the GNU General Public License
     *  along with this program; if not, write to the Free Software
     *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
     *
-->
<xsl:stylesheet  version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="text" encoding="UTF-8"/>
  <xsl:strip-space elements="*"/>

  <xsl:template match="/eurephia">
    <xsl:choose>
      <xsl:when test="$view = 'list'">
        <xsl:apply-templates select="blacklist" mode="list"/>
      </xsl:when>
      <xsl:when test="$view = 'details' or $view = 'details2'">
        <xsl:apply-templates select="blacklist" mode="details"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:message terminate="yes">Invalid view: <xsl:value-of select="$view"/></xsl:message>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>


  <!--                    -->
  <!--   Simple listing   -->
  <!--                    -->

  <xsl:template match="blacklist" mode="list">
    <xsl:text>   ID  Reference                                                    Registered&#10;</xsl:text>
    <xsl:text>-------------------------------------------------------------------------------&#10;</xsl:text>
    <xsl:apply-templates select="username|certificate|ipaddress" mode="list"/>
    <xsl:text>-------------------------------------------------------------------------------&#10;</xsl:text>
  </xsl:template>

  <xsl:template match="blacklist/username" mode="list">
    <xsl:text> ** Username&#10;</xsl:text>
    <xsl:apply-templates select="blacklisted" mode="list"/>
  </xsl:template>

  <xsl:template match="blacklist/username/blacklisted" mode="list">
   <xsl:text> </xsl:text>
    <xsl:call-template name="right-align">
      <xsl:with-param name="value" select="@blid"/>
      <xsl:with-param name="width" select="4"/>
    </xsl:call-template><xsl:text>  </xsl:text>
    <xsl:call-template name="left-align">
      <xsl:with-param name="value" select="username"/>
      <xsl:with-param name="width" select="59"/>
    </xsl:call-template><xsl:text>  </xsl:text>

    <xsl:call-template name="right-align">
      <xsl:with-param name="value" select="registered"/>
      <xsl:with-param name="width" select="10"/>
    </xsl:call-template>
    <xsl:text>&#10;</xsl:text>
  </xsl:template>

  <xsl:template match="blacklist/certificate" mode="list">
    <xsl:text> ** Certificate&#10;</xsl:text>
    <xsl:apply-templates select="blacklisted" mode="list"/>
  </xsl:template>

  <xsl:template match="blacklist/certificate/blacklisted" mode="list">
    <xsl:text> </xsl:text>
    <xsl:call-template name="right-align">
      <xsl:with-param name="value" select="@blid"/>
      <xsl:with-param name="width" select="4"/>
    </xsl:call-template><xsl:text>  </xsl:text>
    <xsl:call-template name="left-align">
      <xsl:with-param name="value" select="certificate"/>
      <xsl:with-param name="width" select="59"/>
    </xsl:call-template><xsl:text>  </xsl:text>

    <xsl:call-template name="right-align">
      <xsl:with-param name="value" select="registered"/>
      <xsl:with-param name="width" select="10"/>
    </xsl:call-template>
    <xsl:text>&#10;</xsl:text>
  </xsl:template>

  <xsl:template match="blacklist/ipaddress" mode="list">
    <xsl:text> ** IP Address&#10;</xsl:text>
    <xsl:apply-templates select="blacklisted" mode="list"/>
  </xsl:template>

  <xsl:template match="blacklist/ipaddress/blacklisted" mode="list">
    <xsl:text> </xsl:text>
    <xsl:call-template name="right-align">
      <xsl:with-param name="value" select="@blid"/>
      <xsl:with-param name="width" select="4"/>
    </xsl:call-template><xsl:text>  </xsl:text>
    <xsl:call-template name="left-align">
      <xsl:with-param name="value" select="ipaddress"/>
      <xsl:with-param name="width" select="59"/>
    </xsl:call-template><xsl:text>  </xsl:text>

    <xsl:call-template name="right-align">
      <xsl:with-param name="value" select="registered"/>
      <xsl:with-param name="width" select="10"/>
    </xsl:call-template>
    <xsl:text>&#10;</xsl:text>
  </xsl:template>


  <!--                    -->
  <!--  Detailed listing  -->
  <!--                    -->

  <xsl:template match="blacklist" mode="details">
    <xsl:text>   ID  Reference&#10;</xsl:text>
    <xsl:text>       Registered first time                                     Last accessed&#10;</xsl:text>
    <xsl:text>-------------------------------------------------------------------------------&#10;</xsl:text>
    <xsl:apply-templates select="username|certificate|ipaddress" mode="details"/>
    <xsl:text>-------------------------------------------------------------------------------&#10;</xsl:text>
  </xsl:template>

  <xsl:template match="blacklist/username" mode="details">
    <xsl:text> ** Username&#10;</xsl:text>
    <xsl:apply-templates select="blacklisted" mode="details"/>
  </xsl:template>

  <xsl:template match="blacklist/username/blacklisted" mode="details">
   <xsl:text> </xsl:text>
    <xsl:call-template name="right-align">
      <xsl:with-param name="value" select="@blid"/>
      <xsl:with-param name="width" select="4"/>
    </xsl:call-template><xsl:text>  </xsl:text>

    <xsl:call-template name="left-align">
      <xsl:with-param name="value" select="username"/>
      <xsl:with-param name="width" select="59"/>
    </xsl:call-template><xsl:text>         </xsl:text>
    <xsl:text>&#10;       </xsl:text>

    <xsl:call-template name="left-align">
      <xsl:with-param name="value" select="registered"/>
      <xsl:with-param name="width" select="19"/>
    </xsl:call-template>
    <xsl:text>                                 </xsl:text>
    <xsl:call-template name="left-align">
      <xsl:with-param name="value" select="last_accessed"/>
      <xsl:with-param name="width" select="19"/>
    </xsl:call-template>
    <xsl:text>&#10;&#10;</xsl:text>
  </xsl:template>

  <xsl:template match="blacklist/certificate" mode="details">
    <xsl:text> ** Certificate&#10;</xsl:text>
    <xsl:apply-templates select="blacklisted" mode="details"/>
  </xsl:template>

  <xsl:template match="blacklist/certificate/blacklisted" mode="details">
   <xsl:text> </xsl:text>
    <xsl:call-template name="right-align">
      <xsl:with-param name="value" select="@blid"/>
      <xsl:with-param name="width" select="4"/>
    </xsl:call-template><xsl:text>  </xsl:text>

    <xsl:call-template name="left-align">
      <xsl:with-param name="value" select="certificate"/>
      <xsl:with-param name="width" select="59"/>
    </xsl:call-template><xsl:text>         </xsl:text>
    <xsl:text>&#10;       </xsl:text>

    <xsl:call-template name="left-align">
      <xsl:with-param name="value" select="registered"/>
      <xsl:with-param name="width" select="19"/>
    </xsl:call-template>
    <xsl:text>                                 </xsl:text>
    <xsl:call-template name="left-align">
      <xsl:with-param name="value" select="last_accessed"/>
      <xsl:with-param name="width" select="19"/>
    </xsl:call-template>
    <xsl:text>&#10;&#10;</xsl:text>
  </xsl:template>

  <xsl:template match="blacklist/ipaddress" mode="details">
    <xsl:text> ** IP Address&#10;</xsl:text>
    <xsl:apply-templates select="blacklisted" mode="details"/>
  </xsl:template>

  <xsl:template match="blacklist/ipaddress/blacklisted" mode="details">
   <xsl:text> </xsl:text>
    <xsl:call-template name="right-align">
      <xsl:with-param name="value" select="@blid"/>
      <xsl:with-param name="width" select="4"/>
    </xsl:call-template><xsl:text>  </xsl:text>

    <xsl:call-template name="left-align">
      <xsl:with-param name="value" select="ipaddress"/>
      <xsl:with-param name="width" select="59"/>
    </xsl:call-template><xsl:text>         </xsl:text>
    <xsl:text>&#10;       </xsl:text>

    <xsl:call-template name="left-align">
      <xsl:with-param name="value" select="registered"/>
      <xsl:with-param name="width" select="19"/>
    </xsl:call-template>
    <xsl:text>                                 </xsl:text>
    <xsl:call-template name="left-align">
      <xsl:with-param name="value" select="last_accessed"/>
      <xsl:with-param name="width" select="19"/>
    </xsl:call-template>
    <xsl:text>&#10;&#10;</xsl:text>
  </xsl:template>





  <!--                    -->
  <!-- Internal helpers   -->
  <!--                    -->

  <xsl:template name="left-align">
    <xsl:param name="value"/>
    <xsl:param name="width"/>
    <xsl:value-of select="substring(concat($value, '                                                                                '), 1, $width)"/>
  </xsl:template>

  <xsl:template name="right-align">
    <xsl:param name="value"/>
    <xsl:param name="width"/>
    <xsl:value-of select="concat(substring('                                                                                ', 1, $width - string-length($value)), substring($value, 1, $width))"/>
  </xsl:template>

</xsl:stylesheet>
