<?xml version="1.0"?>
<!--
     *
     *  GPLv2 only - Copyright (C) 2008 - 2012
     *               David Sommerseth <dazo@users.sourceforge.net>
     *
     *  This program is free software; you can redistribute it and/or
     *  modify it under the terms of the GNU General Public License
     *  as published by the Free Software Foundation; version 2
     *  of the License.
     *
     *  This program is distributed in the hope that it will be useful,
     *  but WITHOUT ANY WARRANTY; without even the implied warranty of
     *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     *  GNU General Public License for more details.
     *
     *  You should have received a copy of the GNU General Public License
     *  along with this program; if not, write to the Free Software
     *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
     *
-->
<xsl:stylesheet  version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="text" encoding="UTF-8"/>
  <xsl:strip-space elements="*"/>

  <xsl:template match="/eurephia">
    <xsl:apply-templates select="certificates|UserAccount/Account/certificates"/>
  </xsl:template>

  <xsl:template match="/eurephia/certificates|/eurephia/UserAccount/Account/certificates">
    <xsl:text>  ID (D) Common name                                              Organisation&#10;</xsl:text>
    <xsl:text>         e-mail                                                     Registered&#10;</xsl:text>
    <xsl:if test="$view_digest = '1'">
      <xsl:text>         Certificate SHA1 digest&#10;</xsl:text>
    </xsl:if>
    <xsl:if test="$firewall = '1'">
      <xsl:text>         Firewall access profile                                FW Destination&#10;</xsl:text>
    </xsl:if>
    <xsl:text> ------------------------------------------------------------------------------&#10;</xsl:text>
    <xsl:apply-templates select="certificate"/>
    <xsl:text> ------------------------------------------------------------------------------&#10;</xsl:text>
  </xsl:template>

  <xsl:template match="certificates/certificate">
    <xsl:text> </xsl:text>
    <xsl:call-template name="right-align">
      <xsl:with-param name="value" select="@certid"/>
      <xsl:with-param name="width" select="3"/>
    </xsl:call-template>
    <xsl:text> </xsl:text>
    <xsl:call-template name="left-align">
      <xsl:with-param name="value" select="concat('(',@depth,')')"/>
      <xsl:with-param name="width" select="3"/>
    </xsl:call-template>
    <xsl:text> </xsl:text>
    <xsl:call-template name="left-align">
      <xsl:with-param name="value" select="common_name"/>
      <xsl:with-param name="width" select="35"/>
    </xsl:call-template>
    <xsl:text> </xsl:text>
    <xsl:call-template name="right-align">
      <xsl:with-param name="value" select="organisation"/>
      <xsl:with-param name="width" select="33"/>
    </xsl:call-template>
    <xsl:text>&#10;         </xsl:text>
    <xsl:call-template name="left-align">
      <xsl:with-param name="value" select="email"/>
      <xsl:with-param name="width" select="49"/>
    </xsl:call-template>
    <xsl:text> </xsl:text>
    <xsl:call-template name="right-align">
      <xsl:with-param name="value" select="@registered"/>
      <xsl:with-param name="width" select="19"/>
    </xsl:call-template>
    <xsl:if test="$view_digest = '1'">
      <xsl:text>&#10;         </xsl:text>
      <xsl:call-template name="left-align">
        <xsl:with-param name="value" select="digest"/>
        <xsl:with-param name="width" select="60"/>
      </xsl:call-template>
    </xsl:if>
    <xsl:if test="$firewall = '1'">
      <xsl:text>&#10;         </xsl:text>
      <xsl:call-template name="left-align">
        <xsl:with-param name="value" select="access_profile"/>
        <xsl:with-param name="width" select="44"/>
      </xsl:call-template>
      <xsl:text> </xsl:text>
      <xsl:call-template name="right-align">
        <xsl:with-param name="value" select="access_profile/@fwdestination"/>
        <xsl:with-param name="width" select="24"/>
      </xsl:call-template>
    </xsl:if>
    <xsl:text>&#10;</xsl:text>
    <xsl:if test="last() > position()">
          <xsl:text>&#10;</xsl:text>
    </xsl:if>
  </xsl:template>


  <xsl:template name="left-align">
    <xsl:param name="value"/>
    <xsl:param name="width"/>
    <xsl:value-of select="substring(concat($value, '                                                                                '), 1, $width)"/>
  </xsl:template>

  <xsl:template name="right-align">
    <xsl:param name="value"/>
    <xsl:param name="width"/>
    <xsl:value-of select="concat(substring('                                                                                ', 1, $width - string-length($value)), $value)"/>
  </xsl:template>

</xsl:stylesheet>
