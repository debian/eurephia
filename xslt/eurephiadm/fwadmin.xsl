<?xml version="1.0"?>
<!--
     *
     *  GPLv2 only - Copyright (C) 2008 - 2012
     *               David Sommerseth <dazo@users.sourceforge.net>
     *
     *  This program is free software; you can redistribute it and/or
     *  modify it under the terms of the GNU General Public License
     *  as published by the Free Software Foundation; version 2
     *  of the License.
     *
     *  This program is distributed in the hope that it will be useful,
     *  but WITHOUT ANY WARRANTY; without even the implied warranty of
     *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     *  GNU General Public License for more details.
     *
     *  You should have received a copy of the GNU General Public License
     *  along with this program; if not, write to the Free Software
     *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
     *
-->
<xsl:stylesheet  version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="text" encoding="UTF-8"/>
  <xsl:strip-space elements="*"/>

  <xsl:template match="/eurephia">
    <xsl:choose>
      <xsl:when test="$view = 'list'">
        <xsl:apply-templates select="firewall_profiles" mode="list"/>
      </xsl:when>
      <xsl:when test="$view = 'details'">
        <xsl:apply-templates select="firewall_profiles" mode="details"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:message terminate="yes">Invalid view: <xsl:value-of select="$view"/></xsl:message>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="/eurephia/firewall_profiles" mode="list">
    <xsl:text>   ID Firewall profile       Description                                Users&#10;</xsl:text>
    <xsl:text> ------------------------------------------------------------------------------&#10;</xsl:text>
    <xsl:apply-templates select="profile" mode="list"/>
    <xsl:text> ------------------------------------------------------------------------------&#10;</xsl:text>
  </xsl:template>

  <xsl:template match="firewall_profiles/profile" mode="list">
    <xsl:text>  </xsl:text>
    <xsl:call-template name="right-align">
      <xsl:with-param name="value" select="@accessprofile"/>
      <xsl:with-param name="width" select="3"/>
    </xsl:call-template><xsl:text> </xsl:text>
    <xsl:call-template name="left-align">
      <xsl:with-param name="value" select="firewall_destination"/>
      <xsl:with-param name="width" select="22"/>
    </xsl:call-template><xsl:text> </xsl:text>
    <xsl:call-template name="left-align">
      <xsl:with-param name="value" select="description"/>
      <xsl:with-param name="width" select="43"/>
    </xsl:call-template><xsl:text> </xsl:text>
    <xsl:call-template name="right-align">
      <xsl:with-param name="value" select="count(granted_accesses/access)"/>
      <xsl:with-param name="width" select="4"/>
    </xsl:call-template>
    <xsl:text>&#10;</xsl:text>
  </xsl:template>


  <xsl:template match="/eurephia/firewall_profiles" mode="details">
    <xsl:text>   ID Firewall profile: Description                                           &#10;</xsl:text>
    <xsl:text>      [uid] Username / Common name / Organisation                             &#10;</xsl:text>
    <xsl:text>            email                                                    Registered&#10;</xsl:text>
    <xsl:text>            [certid] Digest                                                    &#10;</xsl:text>
    <xsl:text> ------------------------------------------------------------------------------&#10;</xsl:text>
    <xsl:apply-templates select="profile" mode="details"/>
    <xsl:text> ------------------------------------------------------------------------------&#10;</xsl:text>
  </xsl:template>

  <xsl:template match="firewall_profiles/profile" mode="details">
    <xsl:text> *</xsl:text>
    <xsl:call-template name="right-align">
      <xsl:with-param name="value" select="@accessprofile"/>
      <xsl:with-param name="width" select="3"/>
    </xsl:call-template><xsl:text> </xsl:text>
    <xsl:value-of select="firewall_destination"/>: <xsl:value-of select="description"/>
    <xsl:text>&#10;</xsl:text> 
    <xsl:apply-templates select="granted_accesses/access"/>
    <xsl:text>&#10;</xsl:text> 
    <xsl:if test="last() > position()">
          <xsl:text>&#10;</xsl:text> 
    </xsl:if>
 </xsl:template>

  <xsl:template match="firewall_profiles/profile/granted_accesses/access">
    <xsl:text>&#10;</xsl:text>
    <xsl:text>      [</xsl:text>
    <xsl:call-template name="right-align">
      <xsl:with-param name="value" select="username/@uid"/>
      <xsl:with-param name="width" select="4"/>
    </xsl:call-template>
    <xsl:text>] </xsl:text>
    <xsl:call-template name="left-align">
      <xsl:with-param name="value" 
                      select="concat(username,' / ',certificate/common_name,' / ',certificate/organisation)"/>
      <xsl:with-param name="width" select="66"/>
    </xsl:call-template>
    <xsl:text>&#10;             </xsl:text>
    <xsl:call-template name="left-align">
      <xsl:with-param name="value" select="certificate/email"/>
      <xsl:with-param name="width" select="45"/>
    </xsl:call-template>
    <xsl:text> </xsl:text>
    <xsl:call-template name="right-align">
      <xsl:with-param name="value" select="certificate/@registered"/>
      <xsl:with-param name="width" select="19"/>
    </xsl:call-template>
    <xsl:text>&#10;             [</xsl:text>
    <xsl:value-of select="certificate/@certid"/>] <xsl:value-of select="certificate/digest"/>
    <xsl:if test="last() > position()">
          <xsl:text>&#10;</xsl:text> 
    </xsl:if>
  </xsl:template>
  
  <xsl:template name="left-align">
    <xsl:param name="value"/>
    <xsl:param name="width"/>
    <xsl:value-of select="substring(concat($value, '                                                                                '), 1, $width)"/>
  </xsl:template>

  <xsl:template name="right-align">
    <xsl:param name="value"/>
    <xsl:param name="width"/>
    <xsl:value-of select="concat(substring('                                                                                ', 1, $width - string-length($value)), $value)"/>
  </xsl:template>

</xsl:stylesheet>
