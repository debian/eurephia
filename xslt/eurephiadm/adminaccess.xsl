<?xml version="1.0"?>
<!--
     *
     *  GPLv2 only - Copyright (C) 2009 - 2012
     *               David Sommerseth <dazo@users.sourceforge.net>
     *
     *  This program is free software; you can redistribute it and/or
     *  modify it under the terms of the GNU General Public License
     *  as published by the Free Software Foundation; version 2
     *  of the License.
     *
     *  This program is distributed in the hope that it will be useful,
     *  but WITHOUT ANY WARRANTY; without even the implied warranty of
     *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     *  GNU General Public License for more details.
     *
     *  You should have received a copy of the GNU General Public License
     *  along with this program; if not, write to the Free Software
     *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
     *
-->
<xsl:stylesheet  version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="text" encoding="UTF-8"/>
  <xsl:strip-space elements="*"/>

  <xsl:template match="/eurephia">
    <xsl:text>  [uid] Username                       Intf.    Access level&#10;</xsl:text>
    <xsl:text> ------------------------------------------------------------------------------&#10;</xsl:text>
    <xsl:apply-templates select="admin_access_list/user_access"/>
    <xsl:text> ------------------------------------------------------------------------------&#10;</xsl:text>
  </xsl:template>

  <xsl:template match="/eurephia/admin_access_list/user_access">
    <xsl:text>   [</xsl:text>
    <xsl:call-template name="right-align">
      <xsl:with-param name="value" select="username/@uid"/>
      <xsl:with-param name="width" select="3"/>
    </xsl:call-template>
    <xsl:text>] </xsl:text>

    <xsl:call-template name="left-align">
      <xsl:with-param name="value" select="username"/>
      <xsl:with-param name="width" select="30"/>
    </xsl:call-template>
    <xsl:text> </xsl:text>

    <xsl:for-each select="access_levels/access">
      <xsl:if test="position() > 1">
        <xsl:text>                                        </xsl:text>
      </xsl:if>
      <xsl:value-of select="substring(@interface, 1)"/>
      <xsl:text>       </xsl:text>
      <xsl:value-of select="."/>
      <xsl:text>&#10;</xsl:text>
    </xsl:for-each>
    <xsl:if test="(last() > position()) and (count(access_levels/access)>1)">
      <xsl:text>&#10;</xsl:text>
    </xsl:if>
  </xsl:template>


  <xsl:template name="left-align">
    <xsl:param name="value"/>
    <xsl:param name="width"/>
    <xsl:value-of select="substring(concat($value, '                                                                                '), 1, $width)"/>
  </xsl:template>

  <xsl:template name="right-align">
    <xsl:param name="value"/>
    <xsl:param name="width"/>
    <xsl:value-of select="concat(substring('                                                                                ', 1, $width - string-length($value)), $value)"/>
  </xsl:template>

</xsl:stylesheet>
