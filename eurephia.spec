Name:           eurephia
Version:        1.1.0
Release:        1%{?betatag:.%{betatag}}%{?dist}
Summary:        An advanced and flexible OpenVPN user authentication plug-in

Group:          Applications/Internet
License:        GPLv2
URL:            http://www.eurephia.net/
Source0:        http://downloads.sourceforge.net/project/eurephia/eurephia/v1.0/%{name}-%{version}%{?betatag:_%{betatag}}.tar.bz2
Source1:        http://svn.openvpn.net/projects/openvpn/branches/BETA21/openvpn/openvpn-plugin.h
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}{?betatag:_%{betatag}}-root-%(%{__id_u} -n)

BuildRequires:  libxml2-devel libxslt-devel openssl-devel
BuildRequires:  cmake >= 2.6.1
BuildRequires:  sqlite-devel >= 3.0.0
Requires:       openvpn
Requires:       eurephia-sqlite3 = %{version}-%{release}
Obsoletes:      eurephia-iptables < 1.0.1-2
Provides:       eurephia-iptables >= 1.0.1-2

%description
This plug-in enhances OpenVPN by adding user name and password
authentication in addition. An eurephia user account is a combination of
minimum one OpenVPN SSL certificate and a user name with a password
assigned. It is also possible to setup several eurephia user names to use
a shared OpenVPN certificate.

In addition, eurephia will blacklist IP addresses, certificates and user names
on too many failed attempts and it supports dynamic update of iptables rules
which restricts network access per connection.

%package sqlite3
Group:   Applications/Internet
Summary: The eurephia SQLite3 database driver

%description sqlite3
This package contains the SQLite3 database driver for eurephia

%package admin
Group:   Applications/Internet
Summary: The eurephia command line administration utility
Requires: eurephia-sqlite3 = %{version}-%{release}

%description admin
This package contains the command line utility to administer and configure
eurephia

%package init
Group:   Applications/Internet
Summary: Utility for initializing a new eurephia database
Requires: eurephia-sqlite3 = %{version}-%{release}

%description init
This package provides a program which will initialize the eurephia
database for you.  It will guide you through several questions and
save the configuration in the database.  When you have configured
and initialized eurephia, this package should be removed from the
system.

%package utils
Group:   Applications/Internet
Summary: Misc. eurephia utilities

%description utils
This package contains useful utilities when debugging eurephia.
At the moment you will only find eurephia_saltdecode in this
package, which will provide some information about the password
hash salt.


%prep 
%setup -q -n %{name}-%{version}%{?betatag:_%{betatag}}

# This is needed to ship extra until openvpn-plugin.h becomes part of the OpenVPN RPM
cp -v %{SOURCE1} .

%build
# The configure script is not an autotools script, but a cmake wrapper script.
CFLAGS="%{optflags}" ./configure --prefix %{_prefix} --bin-dir %{_bindir} --xslt-path %{_datadir}/eurephia/xslt --plug-in-dir %{_libdir}/eurephia --plug-in --openvpn-src . --fw-iptables --db-sqlite3 --sqlite3-path %{_localstatedir}/lib/eurephia --eurephiadm

# We don't do parallel building, as that might fail.
make VERBOSE=1

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

# Move the eurephia-auth.so file to the default OpenVPN plug-in directory
mkdir -p -m 755 %{buildroot}/%{_libdir}/openvpn
mv %{buildroot}/%{_libdir}/eurephia/eurephia-auth.so %{buildroot}/%{_libdir}/openvpn/

# These files are not installed by default, but we want to package them for Fedora/RHEL
install -p -m 755 utils/eurephia_init %{buildroot}/%{_bindir}
install -p -m 755 utils/eurephia_saltdecode %{buildroot}/%{_bindir}
install -p -m 644 utils/eurephia_init.7 %{buildroot}/%{_mandir}/man7/
install -p -m 644 utils/eurephia_saltdecode.7 %{buildroot}/%{_mandir}/man7/

%clean
rm -rf %{buildroot}


%files
%defattr(-,root,root,-)
%doc LICENSE.txt CREDITS.txt
%{_libdir}/openvpn/eurephia-auth.so
%{_mandir}/man7/eurephia-auth.7.gz
%{_mandir}/man7/eurephia-variables.7.gz
%{_libdir}/eurephia/efw-iptables.so

%files sqlite3
%defattr(-,root,root,-)
%doc LICENSE.txt CREDITS.txt
%dir %{_libdir}/eurephia/
%{_libdir}/eurephia/edb-sqlite.so
%{_localstatedir}/lib/eurephia/
%{_mandir}/man7/edb-sqlite.7.gz

%files init
%defattr(-,root,root,-)
%doc LICENSE.txt CREDITS.txt
%{_bindir}/eurephia_init
%{_mandir}/man7/eurephia_init.7.gz

%files admin
%defattr(-,root,root,-)
%doc LICENSE.txt CREDITS.txt
%{_bindir}/eurephiadm
%dir %{_datadir}/eurephia/
%dir %{_datadir}/eurephia/xslt
%{_datadir}/eurephia/xslt/eurephiadm
%{_mandir}/man7/eurephiadm.7.gz
%{_mandir}/man7/eurephiadm-*.7.gz

%files utils
%defattr(-,root,root,-)
%doc LICENSE.txt CREDITS.txt
%{_bindir}/eurephia_saltdecode
%{_mandir}/man7/eurephia_saltdecode.7.gz

%changelog
* Wed Oct 20 2010 David Sommerseth <dazo@users.sourceforge.net> - 1.0.1-2
- Obsolete the eurephia-iptables RPM and move efw-iptables into eurephia.rpm

* Thu Jul 15 2010 David Sommerseth <davids@redhat.com> - 1.0.0-8
- Fixed silly mistakes
  - Forgot to move over /usr/lib/eurephia from eurephia to eurephia-sqlite3
  - Double '=' in eurephia-init

* Thu Jul 15 2010 David Sommerseth <davids@redhat.com> - 1.0.0-7
- More review fixes
  - Relocated ownership of directories
  - Don't hard-code version number in intra-package deps
  - Use proper fully qualified version number (including revision)

* Thu Jul 15 2010 David Sommerseth <davids@redhat.com> - 1.0.0-6
- Fixed file duplication between eurephia and eurephia-admin

* Thu Jul 15 2010 David Sommerseth <davids@redhat.com> - 1.0.0-5
- More review comments from mattias.ellert@fysast.uu.se
  - Own /usr/lib{,64}/eurephia, /usr/share/eurephia/ and /usr/share/eurephia/xslt
  - Intra-packages use fully qualified versions

* Thu Jul 15 2010 David Sommerseth <davids@redhat.com> - 1.0.0-4
- Review comments from mattias.ellert@fysast.uu.se
  - Better way to copy source1
  - Removed -b0 from setup macro

* Thu Jul  8 2010 David Sommerseth <davids@redhat.com> - 1.0.0-3
- Added missing Group tags
- Added stricter cmake version requirement

* Thu Jul  8 2010 David Sommerseth <davids@redhat.com> - 1.0.0-2
- Added missing build dependency for openssl-devel

* Wed Jun 30 2010 David Sommerseth <dazo@users.sourceforge.net> - 1.0.0-1
- Updated and prepared for the eurephia-1.0.0 release

* Wed Nov 18 2009 David Sommerseth <dazo@users.sourceforge.net> - 0.9.6-1.beta
- Updated for eurephia-0.9.6_beta and using openvpn-2.1_rc21 source tree

* Tue Oct  6 2009 David Sommerseth <dazo@users.sourceforge.net> - 0.9.5-1.beta
- Initial eurephia spec file

