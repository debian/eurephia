/* passwd.h  --  Creates SHA512 hash from a clear text password
 *
 *  GPLv2 only - Copyright (C) 2008 - 2012
 *               David Sommerseth <dazo@users.sourceforge.net>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; version 2
 *  of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

/**
 * @file   passwd.h
 * @author David Sommerseth <dazo@users.sourceforge.net>
 * @date   2009-03-21
 *
 * @brief  Functions for generating SHA512 hashes from clear-text values.
 *
 */

#ifndef PASSWD_H
#define PASSWD_H

char *eurephia_pwd_crypt(eurephiaCTX *ctx, const char *key, const char *salt);
char *eurephia_quick_hash(const char *salt, const char *pwd);

#endif
