/* eurephia_directions.h  --  Macro to decide the best search directon on dual-way pointer chains
 *
 *  GPLv2 only - Copyright (C) 2008 - 2012
 *               David Sommerseth <dazo@users.sourceforge.net>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; version 2
 *  of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

/**
 * @file   eurephia_directions.h
 * @author David Sommerseth <dazo@users.sourceforge.net>
 * @date   2008-08-06
 *
 * @brief  Macro to decide the best search directon on dual-way
 *         ring based pointer chains
 *
 */

#ifndef _EUREPHIA_DIRECTIONS_H
#define _EUREPHIA_DIRECTIONS_H

#define DIR_R 'R'		/**< "Right" direction, where the ID number increases (next element)    */
#define DIR_L 'L'		/**< "Left" direction, where the ID number decreases (previous element) */

/**
 * This macro finds the quickest way to go from one element to another one in
 * a circular dual-way pointer chain, based on a numeric id of the elements.
 *
 * @param s starting point on the chain
 * @param d destination point on the chain
 * @param l number of elements in the chain
 *
 * @return Returns DIR_R or DIR_L, depending on what will be the shortest distance.
 */
#define DIRECTION(s,d,l) (s>d ? ((((l-s)+d) > (s-d)) ? DIR_L : DIR_R) : (((d-s) > ((l-d)+s)) ? DIR_L : DIR_R))

#endif
