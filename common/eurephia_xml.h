/* eurephia_xml.h  --  Generic helper functions for XML parsing
 *
 *  GPLv2 only - Copyright (C) 2008 - 2012
 *               David Sommerseth <dazo@users.sourceforge.net>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; version 2
 *  of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

/**
 * @file   eurephia_xml.h
 * @author David Sommerseth <dazo@users.sourceforge.net>
 * @date   2008-12-15
 *
 * @brief  Generic XML parser functions
 *
 */

#ifndef   	EUREPHIA_XML_H_
# define   	EUREPHIA_XML_H_

/**
 * Result types used by eurephiaXML_ResultMsg()
 */
typedef enum _exmlResultType { exmlRESULT = 1, /**< Operation successful. Additional info might be available*/
			       exmlERROR       /**< Operation failed. Error message must be enclosed */
} exmlResultType;

#include <stdarg.h>

#ifdef HAVE_LIBXML2
#include <libxml/tree.h>

/**
 *  Structure which is used when parsing eurephia Result XML documents
 */
typedef struct _eurephiaRESULT {
        exmlResultType resultType; /**< Indicates what kind of result we received */
        const char *message;	   /**< String containing the result message */
        xmlNode *details;	   /**< A result message can attach an XML node with even more
				        detailed information */
} eurephiaRESULT;

/**
 * Simple iterator macro for iterating xmlNode pointers
 *
 * @param start  Pointer to an xmlNode where to start iterating
 * @param itn    An xmlNode pointer which will be used for the iteration.
 */
#define foreach_xmlnode(start, itn)  for( itn = start; itn != NULL; itn = itn->next )

void xmlReplaceChars(xmlChar *str, char s, char r);

char *xmlGetAttrValue(xmlAttr *properties, const char *key);
xmlNode *xmlFindNode(xmlNode *node, const char *key);

int eurephiaXML_CreateDoc(eurephiaCTX *ctx, int format, const char *rootname, xmlDoc **doc, xmlNode **root_n);
xmlNode *eurephiaXML_getRoot(eurephiaCTX *ctx, xmlDoc *doc, const char *nodeset, int min_format);

xmlDoc *eurephiaXML_ResultMsg(eurephiaCTX *ctx, exmlResultType type, xmlNode *info_n, const char *fmt, ... );
unsigned int eurephiaXML_IsResultMsg(eurephiaCTX *ctx, xmlDoc *resxml);
eurephiaRESULT *eurephiaXML_ParseResultMsg(eurephiaCTX *ctx, xmlDoc *resxml);


/**
 * Return the text content of a given xmlNode
 *
 * @param n xmlNode to extract the value from.
 *
 * @return returns a char pointer with the text contents of an xmlNode.
 */
static inline char *xmlExtractContent(xmlNode *n) {
        // FIXME: Should find better way how to return UTF-8 data
        return (char *) (((n != NULL) && (n->children != NULL)) ? n->children->content : NULL);
}

/**
 * Get the text contents of a given xmlNode
 *
 * @param node An xmlNode pointer where to look for the contents
 * @param key  Name of the tag to retrieve the content of.
 *
 * @return Returns a string with the text content, if the node is found.  Otherwise, NULL is returned.
 */
static inline char *xmlGetNodeContent(xmlNode *node, const char *key) {
        return xmlExtractContent(xmlFindNode(node, key));
}

#endif /* HAVE_LIBXML2 */

#endif 	    /* !EUREPHIA_XML_H_ */
