/* eurephia_session.h  --  eurephiaSESSION struct typedef
 *
 *  GPLv2 only - Copyright (C) 2008 - 2012
 *               David Sommerseth <dazo@users.sourceforge.net>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; version 2
 *  of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

/**
 * @file   eurephiadb_session_struct.h
 * @author David Sommerseth <dazo@users.sourceforge.net>
 * @date   2008-11-05
 *
 * @brief  Definition of eurephiaSESSION struct and other
 *         session oriented constants
 *
 */

#ifndef   	EUREPHIADB_SESSION_STRUCT_H_
#define   	EUREPHIADB_SESSION_STRUCT_H_

/**
 * Defines the different session types available
 */
typedef enum { stAUTHENTICATION, /**< A pure authentication session, should only be used during auth. */
	       stSESSION	 /**< A user session, may contain variables related to users session */
} sessionType;

#define SESSION_NEW        1	/**< The session is newly created */
#define SESSION_EXISTING   2	/**< The session is reloaded from a previously saved session */
#define SESSION_REGISTERED 3	/**< The session is registered as logged in */
#define SESSION_LOGGEDOUT  4	/**< The user logged out and the session is closed */

#define SESSVAL_NEW      10	/**< Save a new session value in the database */
#define SESSVAL_UPDATE   11	/**< Update an existing session value in the database */
#define SESSVAL_DELETE   12	/**< Delete a session value */

/**
 * Struct which contains session unique data
 */
typedef struct {
        char *sessionkey;	/**< A unique session key. */
        int sessionstatus;	/**< Must be SESSION_NEW, SESSION_EXISTING,
				  *  SESSION_REGISTERED or SESSION_LOGGEDOUT */
        sessionType type;	/**< Must be stAUTHENTICATION or stSESSION */
        eurephiaVALUES *sessvals; /**< Values unique for this session only */
} eurephiaSESSION;

#endif 	    /* !EUREPHIADB_SESSION_STRUCT_H_ */
